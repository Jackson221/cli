# Command-line Interface Module

This module provides various functions for the command line interface, reguardless of whether that is on a Linux TTY or a game engine drop-down console.

# Dependancies

Depends upon the module3D test and string_manip modules.

# Use

Simply add the files to your build system, and provide the parent directory of all depandancies as an include flag (i.e. -I"src/" )

# License

Project is subject to the GNU AGPL version 3 or any later version, AGPL version 3 being found in LICENSE in the project root directory.
